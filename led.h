#ifndef LED_H                
#define LED_H

//
// Fuctions to control the board LED
// E7012E - Group 3
//

/*
 * Initialize the configuration of the LED 
 */
void 
init_led();


/*
 * Test the working of the LED making it blinking
 */
void 
test_led();

#endif
