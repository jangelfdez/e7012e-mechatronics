
#define	__AVR_ATMEGA128__	1

/* CPU frequency */
#define OSCSPEED	16000000		/* in Hz */

/* UART baud rate */
#define UART_BAUD  19200

/* BIT OPS */
#define BIT_SET(a,b) a |=  _BV(b)
#define BIT_CLR(a,b) a &= ~_BV(b)
#define BIT_TOG(a,b) a ^=  _BV(b)

/* LED MACROS */
#define LED_ON	BIT_SET(PORTE,PE4)
#define LED_OFF	BIT_CLR(PORTE,PE4)
#define	LED_TOG BIT_TOG(PORTE,PE4)

