//
// Fuctions to control the button available in the board
// E7012E - Group 3
//

#include "avr/io.h"
#include "defines.h"
#include "button.h"

/*
 * Initialize the configuration of the button 
 */
void init_button()
{
	//Set PE5 as input
	BIT_CLR(DDRE,PE5);
}

/*
 * Obtains the value of the button 
 */
int read_button()
{
	return PINE & 0x20;
}

