#ifndef MOTORS_H                
#define MOTORS_H

//
// Fuctions to control the PWM generation for the front wheels servomotor
// and for the DC drive motor.
// E7012E - Group 3
//

/*
 * Initialize the configuration of the two PWM 
 * to control the servomotor and the drive motor.
 */
void
init_motors();

/*
 * Test the working of the two motors doing a full
 * range movement between their MAX_VALUE and MIN_VALUE.
 */
void
test_motors();

/* 
 * Initialize the control of the front wheels of the car
 * Configures TMR1 to generate a PWM in Fast PWM Mode.
 */ 
void
init_frontwheels_motor();

/* 
 * Initialize the control of the drive motor of the car
 * Configures TMR3 to generate a PWM in Fast PWM Mode.
 */ 
void
init_drive_motor();

/*
 * Test the full range of movement of the servo motor
 * between MAX_VALUE_FRONTWHEELS and MIN_VALUE_FRONTWHEELS
 */
void 
test_frontwheels_motor();

/*
 * Test the full range of drive motor speed
 * between MAX_VALUE_FRONTWHEELS and MIN_VALUE_FRONTWHEELS
 */
void 
test_drive_motor();

/*
 * Sets the value of the PWM to control the servomotor turn
 * of the front wheels
 */ 
void 
set_front_wheels(int);

/*
 * Sets the value of the PWM to control the speed of the 
 * drive motor 
 */ 
void 
set_drive_motor(int);

#endif
