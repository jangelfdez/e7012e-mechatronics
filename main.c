//
// Line Follower
// 
// E7012E - Group 3
//

#define F_CPU 16000000

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "avr/interrupt.h"
#include "avr/io.h"

#include "util/delay.h"

#include "motors.h"
#include "sensors.h"
#include "led.h"
#include "uart.h"
#include "button.h"
#include "defines.h"

// Define functions
void set_front_wheels_turn(double );
void set_speed(double);
void clean_sensor_values(sensors_data_t*);

// Define PDI variables
double a1 = 0.0, a2 = 0.0, a3 = 0.0, a4 = 0.0;
double x = 0.0, e = 0.0, P = 0.0, I = 0.0, D = 0.0, u= 0.0 , e_old= 0.0;

double Ts = 0.001;

double kp = 0.8; 
double ki = 0.001; 
double kd = 0.08;

int speedStep = 0;

FILE uart_str = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);

/*
 * Interruption routine to attend the press of the button
 */ 
ISR(INT5_vect)
{
	cli();
	speedStep += 1;
	_delay_ms(200);

	printf("Speed %u", speedStep);
	sei();
}


int main(void)
 {
	// Turn on interruption INT5
	EIMSK |= (1 << INT5);
	sei();

	// Init all the systems
	init_led();
	init_motors();
	init_sensors();
		
	uart_init(UART_BAUD);
	
	// Configure the standard input and output for printf
	stdout = stdin = &uart_str;
	
	// Remove the watchdog
	asm("WDR");		

	printf("\n\n\n\t E7012E - Team 3\n");
	printf("\n\n############################################\n\n");
	
	// Set the structure to hold the data of the sensors	
	sensors_data_t* buf = (sensors_data_t*) malloc(sizeof(sensors_data_t));

	//Read the sensors over the line and discards the first value
	read_sensors(buf); read_sensors(buf);

	while(1){

		read_sensors(buf);
		clean_sensor_values(buf);
		printf("\n");
	
		// Check if we are out of the lines to come back. Until we are not again in the line (we read
		// black) the PDI is not running
		if ( buf->sensor_1 > 250 && buf->sensor_2 > 250 && buf->sensor_3 > 250 && buf->sensor_4 > 250)
		{
			if( x > 0)
			{
				set_front_wheels_turn(-4.0);
				continue;
			}
			else
			{
				set_front_wheels_turn(4.0);
				continue;
			}
		}
	
		// Normalize the values read from the sensor between 0 - 1
		a1 = (( (double) buf->sensor_1 / 255.0 * -5.0) + 5.0 )/2.5;
 		a2 = (( (double) buf->sensor_2 / 255.0 * -5.0) + 5.0 )/2.5;
		a3 = (( (double) buf->sensor_3 / 255.0 * -5.0) + 5.0 )/2.5;
		a4 = (( (double) buf->sensor_4 / 255.0 * -5.0) + 5.0 )/2.5;

		// Distance from the middle point
		x = 2.0 * (( 2.5 * a1 + 5.0 * a2 + 7.5 * a3 + 10.0 * a4)/ ( a1 + a2 + a3 + a4 + 0.0001) - 6.25);

		// Using the PDI
		// Error
		e = -x;
		// Proportional term
		P = e;
		// Integral term
		I =0.5*I + e * Ts;	
		// Differential term
		D = (e - e_old) / Ts;
		// Control action signal
		u = kp * P + ki * I + kd * D;
		e_old = e;

		printf("x = %f ", x);
		printf("u = %0.2f ", u);
		printf("P = %0.2f ", P);
		printf("I = %0.2f ", I);
		printf("D = %0.2f ", D);
		
		// Set the movement of the wheels and motor
		set_front_wheels_turn(u);
		set_speed(x);	

		printf("\n");
	}
} 

/*
 * Sets the speed of the drive motor based on the position
 * of the car related to the line
 */
void set_speed(double x)
{
	int val = 0;

	// Security check to have the values between the limits
	if ( x > 7.5)
	{
		x = 7.5;
	} 
	else if (x < -7.5)
	{
		x = -7.5;
	}

	// Lineas response of the car based on the position x
	if ( x >= 0)
	{
		val = 400.0 + speedStep - (400.0 + speedStep - 399)/7.5 * x;
	}
	else if( x < 0)
	{
		val = 400.0 + speedStep + (400.0 + speedStep - 399)/7.5 * x;
	}	
	
	printf("Speed = %d ", val );

	set_drive_motor( val );
}


/*
 * Sets the position of the front wheels based on the action response
 * of the PDI
 */
void set_front_wheels_turn(double pwm)
{
	int val = 0;
		
	double sum = 1.5 + 0.1 * pwm;

	// Security check to have the values between the limits
	if ( sum > 1.9) 
	{
		sum = 1.9;
	} 
	else if (sum < 1.1)
	{
		sum = 1.1;
	}

	// A third degree aproximation based on the response function of the wheels	
	val = sum * sum * sum * 820.825 + sum * sum * (-3584.3) + sum * 5262.2 - 2468.9;

	printf("PWM = %d ", val );

	set_front_wheels( val );
}


/*
 * Removes the fluctuactions on the sensors value due to the imperfections
 * on the path or differences in the hardware. 
 */
void clean_sensor_values(sensors_data_t* buf)
{
        // Set black threshold
		if ( buf->sensor_1 <= 200) {
 			buf->sensor_1 = 128;
		}

		if ( buf->sensor_2 <= 200) {
 			buf->sensor_2 = 128;
		}

		if ( buf->sensor_3 <= 200) {
 			buf->sensor_3 = 128;
		}

		if ( buf->sensor_4 <= 200) {
 			buf->sensor_4 = 128;
		}
		
		// Set white threshold
		if ( buf->sensor_1 > 200) {
 			buf->sensor_1 = 255;
		}

		if ( buf->sensor_2 > 200) {
 			buf->sensor_2 = 255;
		}

		if ( buf->sensor_3 > 200) {
 			buf->sensor_3 = 255;
		}

		if ( buf->sensor_4 > 200) {
 			buf->sensor_4 = 255;
		}
}
