#ifndef UART_H                
#define UART_H

//
// Fuctions to control the serial communication with the PC
// through the UART port.
// E7012E - Group 3
//

/*
 * Size of internal line buffer used by uart_getchar().
 */
#define RX_BUFSIZE 80

/*
 * Perform UART startup initialization.
 */
void 
uart_init(uint32_t Baud);

/*
 * Send one character to the UART.
 */
int	
uart_putchar(char c, FILE *stream);

/*
 * Receive one character from the UART.  The actual reception is
 * line-buffered, and one character is returned from the buffer at
 * each invokation.
 */
int	
uart_getchar(FILE *stream);

/*
 * Receive a character from the UART
 */ 
unsigned char 
UART_Receive(void);

/*
 * Send a character through the UART
 */
void 
UART_Transmit(unsigned char Data);



#endif
