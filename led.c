//
// Fuctions to control the board LED
// E7012E - Group 3
//
#define F_CPU 16000000

#include "util/delay.h"
#include "avr/io.h"

#include "led.h"

/*
 * Initialize the configuration of the LED 
 */
void init_led()
{
	// Configures PE4 as an OUTPUT
	DDRE |= ( 1 << PE4);
	// Sets the LED ON
	PORTE |= ( 1 << PE4);
}

/*
 * Test the working of the LED toggling it
 */
void test_led()
{
	PORTE &= (0 << PE4);
	_delay_ms(10000);
	
	PORTE |= (1 << PE4);
	_delay_ms(10000);
}
