//
// Fuctions to control the PWM generation for the front wheels servomotor
// and for the DC drive motor.
// E7012E - Group 3
//
#define F_CPU 16000000

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "avr/io.h"
#include "avr/interrupt.h"
#include "util/delay.h"

#include "sensors.h"


/*
 * Initialize the configuration of the ADC
 * to read the values from the IR sensors.
 */
void init_sensors()
{
	// Set ADC prescaler to 128 -> 16MHz/128 = 125KHz
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); 
	
	// Uses AVcc as voltage reference
	ADMUX |= (1 << REFS0);
	
	// Left align of the conversion result.
	ADMUX |= (1 << ADLAR);

	// Enable ADC converter
	ADCSRA |= (1 << ADEN);
}

/*
 * Starts a new reading of the values
 * received.
 */
void read_sensors(sensors_data_t* buffer)
{
	// Select Sensor 1
	ADMUX = 0b01100000;

	// Start a new reading
	ADCSRA |= (1 << ADSC);
		
	// While until is ready
	while (ADCSRA & (1<<ADSC)){
	}

	// Read value of the conversion 
	// Only 8 bit resolution is used
	buffer->sensor_1= ADCH;
	printf("S1 = %u ", buffer->sensor_1 );


	// Select Sensor 2
	ADMUX = 0b01100001;

	// Start a new reading
	ADCSRA |= (1 << ADSC);
		
	// While until is ready
	while (ADCSRA & (1<<ADSC)){
	}

	// Read value of the conversion 
	// Only 8 bit resolution is used
	buffer->sensor_2 = ADCH;
	printf("S2= %u ", buffer->sensor_2 );

	// Select Sensor 3
	ADMUX = 0b01100010;

	// Start a new reading
	ADCSRA |= (1 << ADSC);
		
	// While until is ready
	while (ADCSRA & (1<<ADSC)){
	}

	// Read value of the conversion 
	// Only 8 bit resolution is used
	buffer->sensor_3 = ADCH;
	printf("S3= %u ", buffer->sensor_3 );

	// Select Sensor 4
	ADMUX = 0b01100011;

	// Start a new reading
	ADCSRA |= (1 << ADSC);
		
	// While until is ready
	while (ADCSRA & (1<<ADSC)){
	}

	// Read value of the conversion 
	// Only 8 bit resolution is used
	buffer->sensor_4 = ADCH;
	printf("S4= %u ", buffer->sensor_4 );
}

/*
 * Stops a new reading of the values
 * received.
 */
void stop_sensors()
{
	// Stop the ADC converter
	ADCSRA &= ~(1 << ADEN);	
}
