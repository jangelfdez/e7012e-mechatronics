//
// Fuctions to control the PWM generation for the front wheels servomotor
// and for the DC drive motor.
// E7012E - Group 3
//
#define F_CPU 16000000

#include "util/delay.h"
#include "avr/io.h"

#include "motors.h"

const int FREQUENCY_PWM = 5000;			// F = 50Hz, T = 20ms

const int MAX_VALUE_FRONTWHEELS = 220;	// Right max. turn value
const int MIN_VALUE_FRONTWHEELS = 75;	// Left max. turn value
const int MEDIUM_VALUE_FRONTWHEELS = 130;	// Straight value ((MAX_VALUE_FRONTWHEELS + MIN_VALUE_FRONTWHEELS) / 2)

const int MAX_VALUE_DRIVEMOTOR = 420;		// Right max. turn value
const int MIN_VALUE_DRIVEMOTOR = 300;		// Left max. turn value
const int MEDIUM_VALUE_DRIVEMOTOR = 375;		// Straight value ((MAX_VALUE_DRIVEMOTOR + MIN_VALUE_DRIVEMOTOR) / 2)

const int STEP_MOTOR = 3;
const int STEP_SERVO = 50;

/*
 * Initialize the configuration of the two PWM 
 * to control the servomotor and the drive motor.
 */
void init_motors()
{
	init_frontwheels_motor();
	init_drive_motor();

}

/*
 * Test the working of the two motors doing a full
 * range movement between their MAX_VALUE and MIN_VALUE.
 */
void test_motors()
{
	test_frontwheels_motor();
	test_drive_motor();
}

/* 
 * Initialize the control of the front wheels of the car
 * Configures TMR1 to generate a PWM in Fast PWM Mode.
 */ 
void init_frontwheels_motor()
{
	// Configure PWM Output OC1A = PB5
	DDRB = (1 << PB5);
	
	// Configure Timer 1 Counter Register A
	//
	// COM1A1 = 1 ; COM1A0 = 0 -> Clear on compare Match Channel A at TOP
	// COM1B1 = 1 ; COM1B0 = 0 -> Clear on compare Match Channel B at TOP
	// CS12 = 0 ; CS11 = 1 ; CS10 = 1 ; -> Set prescaler x64 
	// WGM13 = 1 ; WGM12 = 1 ; WGM11 = 1 ; WGM10 = 0 -> Waveform Generator Mode, Fast PWM, TOP = ICR1
	TCCR1A = (1 << COM1A1) |( 1 << COM1B1) | ( 1 << WGM11);
    TCCR1B = ( 1 << CS10)| ( 1 <<  CS11) | ( 1 << WGM12) | ( 1 << WGM13);
    
	// Values extracted from equation
	// N = ( Prescaler / CLCKf ) * tmseconds

	// Sets the MAX value of the counter -> Frequency 
	ICR1 = FREQUENCY_PWM;
    
	// Sets the TOP value of the counter -> Duty Cycle
	OCR1A = MEDIUM_VALUE_FRONTWHEELS; 
}


/* 
 * Initialize the control of the drive motor of the car
 * Configures TMR3 to generate a PWM in Fast PWM Mode.
 */ 
void init_drive_motor()
{
	// Configure PWM Output OC3A = PE3
	DDRE = DDRE |(1 << PE3);
	
	// Configure Timer 3 Counter Register A
	//
	// COM3A1 = 1 ; COM3A0 = 0 -> Clear on compare Match Channel A at TOP
	// COM3B1 = 1 ; COM3B0 = 0 -> Clear on compare Match Channel B at TOP
	// CS32 = 0 ; CS31 = 1 ; CS30 = 1 ; -> Set prescaler x64 
	// WGM33 = 1 ; WGM32 = 1 ; WGM31 = 1 ; WGM30 = 0 -> Waveform Generator Mode, Fast PWM, TOP = ICR3
	TCCR3A = (1 << COM3A1) |( 1 << COM3B1) | ( 1 << WGM31);
    TCCR3B = ( 1 << CS30)| ( 1 <<  CS31) | ( 1 << WGM32) | ( 1 << WGM33);
    
	// Values extracted from equation
	// N = ( Prescaler / CLCKf ) * tmseconds

	// Sets the MAX value of the counter -> Frequency 
	ICR3 = FREQUENCY_PWM;
    
	// Sets the TOP value of the counter -> Duty Cycle
	OCR3A = MEDIUM_VALUE_DRIVEMOTOR; 
}


/*
 * Test the full range of movement of the servo motor
 * between MAX_VALUE_FRONTWHEELS and MIN_VALUE_FRONTWHEELS
 */
void test_frontwheels_motor()
{
	// Full left
	while ( OCR1A < MAX_VALUE_FRONTWHEELS )
	{
		OCR1A = OCR1A + STEP_SERVO;
		_delay_ms(500);
		
	}
	
	// Full right
	while ( OCR1A > MIN_VALUE_FRONTWHEELS)
	{
		OCR1A = OCR1A - STEP_SERVO;
		_delay_ms(500);	
	}

	OCR1A = MEDIUM_VALUE_FRONTWHEELS;
}

/*
 * Test the full range of drive motor speed
 * between MAX_VALUE_DRIVEMOTOR and MIN_VALUE_DRIVEMOTOR
 */
void test_drive_motor()
{	
	// Full forward?
	while ( OCR3A < MAX_VALUE_DRIVEMOTOR )
	{
		OCR3A = OCR3A + STEP_MOTOR;
		_delay_ms(100);
		
	}
		
	// Full backward?
	while ( OCR3A > MIN_VALUE_DRIVEMOTOR)
	{
		OCR3A = OCR3A - STEP_MOTOR;
		_delay_ms(100);	
	}	
}

/*
 * Sets the value of the PWM to control the servomotor turn
 * of the front wheels
 */ 
void set_front_wheels(int value)
{
	OCR1A = value;
}

/*
 * Sets the value of the PWM to control the speed of the 
 * drive motor 
 */
void set_drive_motor(int value)
{
	OCR3A = value;
}
