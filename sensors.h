#ifndef SENSORS_H                
#define SENSORS_H

//
// Fuctions to control the work with the IR sensors of car
// E7012E - Group 3
//

struct sensors
{
	unsigned char sensor_1;
	unsigned char sensor_2;
	unsigned char sensor_3;
	unsigned char sensor_4;
};
typedef struct sensors sensors_data_t;


/*
 * Initialize the configuration of the ADC
 * to read the values from the IR sensors.
 */
void 
init_sensors();

/*
 * Starts a new reading of the values
 * received.
 */
void
read_sensors(sensors_data_t*);

/*
 * Stops a new reading of the values
 * received.
 */
void 
stop_sensors();

#endif
