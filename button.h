#ifndef BUTTON_H                
#define BUTTON_H

//
// Fuctions to control the button available in the board
// E7012E - Group 3
//

/*
 * Initialize the configuration of the button 
 */
void 
init_button();


/*
 * Obtains the value of the button 
 */
int 
read_button();

#endif
